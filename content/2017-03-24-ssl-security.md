title: SSL security and trust
date: 2017-03-24
slug: ssl-security
category: misc
tags: https, security
summary: Symantec has been [caught yet again](https://groups.google.com/a/chromium.org/forum/#!topic/blink-dev/eUAKwjihhBs) failing to make its due diligence when issuing certificates: this time it looks like they have failed to properly validate at least 30.000 certificates.
og_image: images/ssl-security/ssl-failure.png

<img style="float:left; padding-right:10px" src="/images/ssl-security/ssl-failure.png" />

Symantec has been [caught yet again](https://groups.google.com/a/chromium.org/forum/#!topic/blink-dev/eUAKwjihhBs) failing to make its due diligence when issuing certificates: this time it looks like they have failed to properly validate at least 30.000 certificates.

In response, the Chrome team announced that it will stop recognizing the extended validation status of all certificates issued by Symantec-owned certificate authorities, and to nullify all currently valid certificates in a staggered approach by decreasing the maximum age of Symantec-issued certs over a series of releases, ending with a validity period of only 9 months by Chrome 64' release.

---

###The web of trust

The problem with certificates authorities is that you need to trust them in order for the whole system to work. That is, you trust that Symantec, or Thawte, Verisign, Equifax and others (all bought by Symantec in the past years) actually checked that whoever requested that certificate is the actual owner of that business.

The issue here is not the validity of the certificates but the trust under which they were issued. EV certificates promise Extended Validation of the legal entity requesting the certificate: check with BBB and double check with Dun & Bradstreet, for instance. If you can skip the 'extended validation'-part by paying more, EV becomes meaningless because a fraudster with deep pockets can get an EV-certificate for his bankofamurica.com-website.

For an example of the process someone goes through to get an EV certificate you can read the great post written by Troy Hunt, [Journey to an extended validation certificate](https://www.troyhunt.com/journey-to-an-extended-validation-certificate/).

---

###TooBigToFail™

The problem is these entities getting TooBigToFail™, which then makes them almost invulnerable to meaningful actions.

By consolidating so many certificate authorities under one umbrella it makes it almost impossible to revoke their certificates without breaking a large part of the Internet. To put this into perspective, Symantec certificates represent more than 30 percent of the Internet's valid certificates by volume in 2015 and around 42% of all certificate validations.

In a perfect world companies who have their root certificates entrusted as part of the TLS core infrastructure need to have better checks and balances than to simply say "oops, we done goofed" after the fact. If they demonstrate - as Symantec has demonstrated - that they can't manage that, their root certificates need to be yanked out of the chain of trust as soon as possible.

It will be interesting to see how the customers who have paid for their certs in good faith react to this move by Google, and hopefully the backlash will make Symantec take better care in the future.

---

###What next?

The question now is what happens next? What do we do to ensure that we're in the clear?

Well, a solution would be to move away from Symantec and the rest and towards [Let's Encrypt](https://letsencrypt.org/), especially if you don't need EV certificates.

But Let's Encrypt really does almost NO validation what-so-ever, I hear you say. As long as you are in control of the web server at said domain, you can get a certificate. How many web servers are compromised on a daily basis at any one domain?

This is only sometimes helpful in practice, but it's important to remember the distinction between "what you promise" and "how much of what you promise you deliver".

Let's Encrypt doesn't promise all that much; their system is pretty much just designed to ensure that the entity that requested the certificate has (or very recently had, their certificates do last a modest period of time after issue) operational control of the host for which the certificate is issued.

That isn't a terribly grand promise: it doesn't imply anything about the real-world owner of the site; tell you whether or not somebody compromised the host, etc. but it has the virtue of both being relatively easy and cheap to automatically verify and of being something where the most common failure mode (compromised host) isn't a threat that SSL is supposed to protect against, no matter how exhaustively vetted (it is supposed to protect the channel between you and the server from 3rd parties, not assure you that the guy running the server is trustworthy); so failures there aren't terribly serious.

To the best of my knowledge, while LE doesn't promise much, they have so far delivered it; and have avoided failures that threaten other people's sites. If my little VPS gets hacked, or my shoddy admin dashboard has an exploit, somebody else getting an LE cert for my site is quite plausible; but that's because somebody else does, indeed, have operational control of my site. Getting an LE cert without demonstrating operational control is what would be really worrisome.

And, that is where Symantec has been trouble: they've been caught issuing enormously powerful certs for high profile domains without the request of the domain holder, which are potent weapons for MiTM attacks. That is bad.