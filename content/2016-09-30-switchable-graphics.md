title: A short on switchable graphics
date: 2016-09-30
slug: switchable-graphics
category: misc
tags: graphics, mobile, nVidia, Optimus, AMD, Intel
summary: With the increase in the number of people using only laptops for their computing needs came new technologies used to provide both power and battery life. Switchable graphics - moving from an integrated GPU to a dedicated one, is one such technologies. But how do they work?

With the increase in the number of people using only laptops for their computing needs came new technologies used to provide both power and battery life. Switchable graphics - moving from an integrated GPU to a dedicated one, is one such technologies. But how do they work?

---

###Per-display graphics card? :fa-television:

This whole post started after we received new laptops at work equipped with both an Intel HD Graphics 530 integrated GPU (iGPU) and an AMD FirePro W5170M (discrete GPU - dGPU). One of my colleagues was wondering if, when running multiple monitors, it is possible to get one or the other of the displays to be driven exclusively by the dGPU.

---

###Sharing is caring

The short answer to the above question is no. It's impossible because only the iGPU is connected to the display; the discrete GPU only renders to video memory, and the integrated GPU displays the image on the screen.

Remember that a graphics chip has two jobs: fill the frame buffer with data and output that data to the monitor. The first and the second job actually happen at the same time, but in different locations in memory; the GPU is actually reading and writing to VRAM at the same time. The part of the memory being currently displayed is called the frame buffer, and the part being filled up is called the back buffer.

On a switchable graphics system the iGPU does both jobs most of the time: it takes commands like "draw a red line from here to there" and sets the value of the pixels along that line to the color red while another part of the iGPU reads the data out to the video port. This second part is actually fairly slow and takes the same amount of time, no matter what data is in video memory. When you operate in high power mode, the dGPU fills the back buffer and the iGPU reads it.

So in a push-pull configuration like this, the dedicated video chip is not ever actually connected to the video port. Instead, it simply feeds data into the frame buffers that the Intel Graphics Processor uses to display the images. Because of that, there's really no way for you to connect one GPU to one monitor - there's literally no connection between the GPU and the monitor at all. All video goes through the iGPU.