title: Of COM, memory and restarts
date: 2017-06-01
slug: com-restarts
category: misc
tags: Windows
summary: If you're old enough to have played with Windows back in the 95/98 era then you might have messed around with .com files. I once stumbled upon a quick way to restart the PC by using a basic .com file, but why did that work, and more importantly, why doesn't it work on more recent versions of the OS?

###A bit of history

Most people might think of EXE files when asked "what is a program on Windows?", but before thta the only programs that existed on Windows land were COM files. The COM format, inherited from CP/M wasn't actually a format, it was just a memory image which got loaded into memory unchanged and then ran from the first byte.

The downside of COM files, and what eventually lead to the creation of EXE was that the programs couldn't be bigger than 64KB (I guess this had something to do with register segments being around 64KB on the 8086), and slowly COM was forgotten.

---

###Of memory and processor instructions

Getting to how to restart a PC using a COM file, I ran accross it by accident, by renaming a .bat file which had something like `cd c:\games` in it to COM, which reseted the PC.

Now, knowing tha COM files are actually memory images, the restart actually makes sense. `cd` disassembles into `arpl [si+0x20],sp` which despite looking like a priviledged instruction, it actually isn't; more than this, it isn't even a valid instruction in real or virtual mode, which causes the illegal instruction exception and restarts the PC.

Another fun fact is that the code for the warmest reboot on PC is `0xcd 0x19` (int 0x19) which jumps back into the BIOS code when it starts looking for the operating system to boot. You could then have files containing `INT 19; (CD 19)` which was the fastest way to soft reboot when the file was run from a MSDOS mode shortcut.

But wait, there's more! This has the side-effect of confusing the NT NTLDR, which spits out a "your computer does not have enough free conventional memory to run Windows NT" error message.
