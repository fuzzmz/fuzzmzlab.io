beautifulsoup4==4.5.1
blinker==1.4
docutils==0.12
feedgenerator==1.9
fontawesome-markdown==0.2.6
# -e git://github.com/fuzzmz/fontawesome-markdown.git@ba308e9e92eae00644cff7292f4565775ca80c41#egg=fontawesome-markdown
Jinja2==2.8
Markdown==2.6.7
MarkupSafe==0.23
pelican==3.6.3
# pelicanfly==0.4.4
-e git://github.com/fuzzmz/pelicanfly.git@85138810659ff96f6e82ba6fc41ff89de27ed566#egg=pelicanfly
Pygments==2.1.3
python-dateutil==2.5.3
pytz==2016.6.1
six==1.10.0
smartypants==1.8.6
Unidecode==0.4.19
