![Build Status](https://gitlab.com/fuzzmz/fuzzmz.gitlab.io/badges/master/build.svg)

----

Welcome to the sources of [fuzz.me.uk], the personal blog of Serban Constantin.

This blog is happily running [Pelican] and is hosted on GitLab pages.

---

## Building locally

To work locally with this project, there are a few options. But let's keep it
simple:

1. Fork, clone or download this project
1. Install pelican
1. Generate the website: `make html`
1. Preview your project: `make serve`

## Contributing

All contributions are welcome, either article ideas or direct write-ups, as long
as you agree with the LICENSE.

All contribution requests should be done via the [issue tracker](https://gitlab.com/fuzzmz/fuzzmz.gitlab.io/issues).

[fuzz.me.uk]: https://fuzz.me.uk
[pelican]: http://blog.getpelican.com/
